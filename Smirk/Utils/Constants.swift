//
//  Constants.swift
//  Smirk
//
//  Created by Moazam Mir on 3/25/21.
//

import Foundation

struct Constants {
    
    struct ImageSettings {
        
        //TODO: Put this in a different file
        static let PAGE_AREA_HEIGHT: Int = 500
        static let PAGE_AREA_WIDTH: Int = 500
        static let FONT_A_HEIGHT: Int = 24
        static let FONT_A_WIDTH: Int = 12
        static let BARCODE_HEIGHT_POS: Int = 70
        static let BARCODE_WIDTH_POS: Int = 110
    }

    struct Keys {
        static let isInitialSetup = "OnboardingComplete"
    }
}
