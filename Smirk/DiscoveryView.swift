//
//  DiscoveryView.swift
//  Smirk
//
//  Created by Moazam Mir on 3/25/21.
//

import SwiftUI

struct DiscoveryView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct DiscoveryView_Previews: PreviewProvider {
    static var previews: some View {
        DiscoveryView()
    }
}
