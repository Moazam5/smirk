//
//  DiscoveryVCRepresentable.swift
//  Smirk
//
//  Created by Moazam Mir on 3/26/21.
//

import SwiftUI

struct DiscoveryVCRepresentable: UIViewControllerRepresentable {
   
    

    func makeUIViewController(context: Context) -> some UIViewController {
        return DiscoveryViewController()
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
    
    
}
