//
//  CameraSelection.swift
//  Smirk
//
//  Created by Moazam Mir on 3/2/21.
//

import SwiftUI

struct FirstSetupView: View {
    @State var completeSetup = false
    @State var isShowPhotoLibrary = false
    @State var isShowPhotoLibrary2 = false
    @State private var iconImage : UIImage?
    @State private var advImage : UIImage?
    @State private var showingAlert = false

    var defaults = UserDefaults.standard

    fileprivate func extractedFunc() -> Text {
        return Text("Choose a logo to print")
    }
    
    var body: some View {
        VStack {
            Spacer()
            ZStack{
                ProgressHeader(firstLabel: "Setup Photo", isFirstSelected: true, secondLabel: "Setup Printer", isSecondSelected: false)
                    .padding()
                    .frame(maxWidth: .infinity )
            }

            Spacer()
            HStack {
                VStack{
                    Text("Choose a logo to print")
                    Text("Select file below")
                        .font(.system(size: 15))

                    Button {  self.isShowPhotoLibrary2 = true } label: {
                        if let image = iconImage {
                            Image(uiImage: image)
                                .resizable()
                        } else {
                            Image(systemName: "square.and.arrow.up")
                                .resizable()
                                .frame(width: 50, height: 50, alignment : .center)
                                .foregroundColor(.gray)
                        }
                    }
                    .sheet(isPresented: $isShowPhotoLibrary2){
                        ImagePicker(sourceType: .photoLibrary, selectedImage: $iconImage)
                    }
                    .frame(width: 300, height: 300)
                    .background(Color.white)
                    .cornerRadius(15)
                    .shadow(color: .init(.sRGB, white: 0, opacity: 0.20), radius: 4, x: 0, y: 4)
                    .padding()
                }

                Spacer()
                
                VStack{
                    Text("Choose an advertisement")
                    Text("Select file below")
                        .font(.system(size: 15))
                    
                    Button {
                        print("Button pressed")
                        self.isShowPhotoLibrary = true
                    } label: {
                        if let image = advImage {
                            Image(uiImage: image)
                                .resizable()
                        } else {
                            Image(systemName: "square.and.arrow.up")
                                .resizable()
                                .foregroundColor(.gray)
                                .frame(width: 50, height: 50, alignment : .center)
                        }
                    }
                    .sheet(isPresented: $isShowPhotoLibrary){
                        ImagePicker(sourceType: .photoLibrary, selectedImage: $advImage)
                    }
                    .frame(width: 300, height: 300)//,// alignment: .leading)
                    .background(Color.white)
                    .cornerRadius(12)
                    .shadow(color: .init(.sRGB, white: 0, opacity: 0.20), radius: 4, x: 0, y: 4)
                    .padding()
                }


            }
            .font(.system(size: 20))
            .foregroundColor(Color.white)
            
            Spacer()
            
                HStack {
                    Spacer()
                    Button("Next") {
                        completeSetup.toggle()
                        let encodedData = try! NSKeyedArchiver.archivedData(withRootObject: iconImage.self, requiringSecureCoding: false)
                        let userDefaults = UserDefaults.standard
                        defaults.set(true, forKey: Constants.Keys.isInitialSetup)
                        userDefaults.set(encodedData, forKey: "iconImage")
                        print("Next button pressed")
                    }
                    .sheet(isPresented: $completeSetup) {
                        PhotoOnlyView() }
                    .frame(width: 150, height: 50, alignment: .center)
                    .background(Color.white)
                    .cornerRadius(10)
                    .padding(.bottom,50)
                    
                }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(LinearGradient(gradient: Gradient(colors: [.white, .blue]),
        startPoint: .top, endPoint: .bottom))
        .ignoresSafeArea()
    }
}

struct CameraSelection_Previews: PreviewProvider {
    static var previews: some View {
        FirstSetupView()
    }
}
