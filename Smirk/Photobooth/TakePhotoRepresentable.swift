//
//  TakePhotoRepresentable.swift
//  Smirk
//
//  Created by Moazam Mir on 3/25/21.
//

import SwiftUI

struct TakePhotoRepresentable: UIViewControllerRepresentable {
  
    func makeUIViewController(context: Context) -> some UIViewController {
        
        return TakePhotoViewController()
    }
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
}

struct TakePhotoRepresentable_Previews: PreviewProvider {
    static var previews: some View {
        TakePhotoRepresentable()
    }
}
