//
//  DiscoverPrinter.swift
//  Smirk
//
//  Created by Moazam Mir on 3/25/21.
//

import SwiftUI

struct DiscoverPrinterView: View {
 //   @Binding var targetPrinterName : String
    
    
    var body: some View {
        
        VStack
        {
            Text("Select Printer")
            DiscoveryVCRepresentable()
                .frame(width: 300, height: 300, alignment: .center)
        
        }
    }
}


//
//struct DiscoverPrinter_Previews: PreviewProvider {
//    static var previews: some View {
//        DiscoverPrinterView()
//    }
//}
