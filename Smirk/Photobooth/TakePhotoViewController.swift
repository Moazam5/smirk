//
//  TakePhotoViewController.swift
//  Smirk
//
//  Created by Moazam Mir on 3/25/21.
// TODO: 1. Use nslog
// 2. Create a View model
// 3. Display printer warning, use this to show to user
// 4. Get rid of DiscoveryViewDelegate
// 5. Update background -- High Priority
// 6. onPtrReceive - Use this



import UIKit

class TakePhotoViewController: UIViewController, DiscoveryViewDelegate, Epos2PtrReceiveDelegate
{
   
    //MARK:- Variables
    var printer: Epos2Printer?
    var valuePrinterSeries: Epos2PrinterSeries = EPOS2_TM_M10
    var valuePrinterModel: Epos2ModelLang = EPOS2_MODEL_ANK
   
    
    //MARK:- View lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIButton(frame:CGRect(x: (view.bounds.width / 2) - 275, y: (view.bounds.height / 2) - 100, width: 500, height: 150))
        
        //TODO:- Change the name of the button
        button.setTitle("FREE PHOTO", for: .normal)
        
        button.layer.cornerRadius = 12
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(red: 75, green: 150, blue: 250, alpha: 1.0) //(named: "92BBE0")
        
        button.addTarget(self, action: #selector(takePicButtonPressed), for: .touchUpInside)
        view.addSubview(button)


        self.imagePicker = ImagePicker2(presentationController: self, delegate: self)


    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let initializeStatus  = initializePrinterObject()
        print("Initialization printer status", initializeStatus)
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        finalizePrinterObject()
    }
    
    
    
    //MARK:- Button Methods
    var imagePicker: ImagePicker2!
    var selectedPic : UIImage?

    
    @objc func takePicButtonPressed()
    {
        print("Take pic button pressed")
        
        //call image picker here, pass the argument to the method below
        self.imagePicker.present(from: view)

//        if runPrinterReceiptSequence(pic: selectedPic!)
//        {
//            print("Completed")
//            //show overlay or success, else show error
//        }
    }
    


    //not needed - find an alernative
    func updateButtonState(_ state: Bool) {
        print("State is ",state)
        
//
//        buttonDiscovery.isEnabled = state
//        buttonLang.isEnabled = state
//        buttonPrinterSeries.isEnabled = state
//        buttonReceipt.isEnabled = state
//        buttonCoupon.isEnabled = state
//
    }
    
    func runPrinterReceiptSequence(pic : UIImage) -> Bool {
        if !createReceiptData(imageName: pic) {
            print("Failed to crate data")
            return false
        }
        
        if !printData() {
            print("Failed to print data")

            return false
        }
        
        return true
    }
    
  //TODO: Do a guard statement to unwrap print
    func printData() -> Bool {
        if printer == nil {
            return false
        }
        
        if !connectPrinter() {
            printer!.clearCommandBuffer()
            print("Error connecting to printer")
            return false
        }
        
        let result = printer!.sendData(Int(EPOS2_PARAM_DEFAULT))
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
      //      MessageView.showErrorEpos(result, method:"sendData")
            printer!.disconnect()
            return false
        }
        
        return true
    }
    
    private func createReceiptData(imageName : UIImage = UIImage(named: "store.png")! ) -> Bool {
        
        var result = EPOS2_SUCCESS.rawValue
        let logoData = imageName
        let textData: NSMutableString = NSMutableString()

        
        
        if printer == nil {
            print("printer nil")
            return false
        }
        
        print(printer)
        //add logo - need to get rid of this
//        result = printer!.addTextAlign(EPOS2_ALIGN_CENTER.rawValue)
//        if result != EPOS2_SUCCESS.rawValue {
//            print("Error adding data to printer")
//
//            return false;
//        }

        print("logo widt is", logoData.size.width, "\nlogo height is", logoData.size.height)
        
        result = printer!.add(logoData, x: 0, y:0,
                              width:Int(logoData.size.width),
                              height:Int(logoData.size.height),
            color:EPOS2_COLOR_1.rawValue,
            mode: EPOS2_MODE_MONO.rawValue,
            halftone:EPOS2_HALFTONE_DITHER.rawValue,
            brightness:Double(EPOS2_PARAM_DEFAULT),
            compress:EPOS2_COMPRESS_AUTO.rawValue)
        
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
            print("Error adding logo")
            return false
        }
        
        result = printer!.addTextAlign(EPOS2_ALIGN_CENTER.rawValue)
        if result != EPOS2_SUCCESS.rawValue {
            print("Error with text align")
            return false;
        }
        
        //Add text
        result = printer!.addFeedLine(1)
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
            return false
        }
        
        
        textData.append("------------------------------\n")
       
        
        result = printer!.addText(textData as String)
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
            print("Error sir")
            return false;
        }

    
        
        let decoded  = UserDefaults.standard.object(forKey: "iconImage") as! Data
        let logo = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as! UIImage
      //  print(decodedTeams.name)

        result = EPOS2_SUCCESS.rawValue
     //   let logo = UserDefaults.standard.value(forKey: "iconImage") as! UIImage?
            result = printer!.addTextAlign(EPOS2_ALIGN_CENTER.rawValue)

        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()

            print("Error adding data to printer")

            return false;
        }
        result = printer!.add(logo, x: 0, y:0,
                              width:Int(500 ),
                              height:Int(500),
            color: EPOS2_COLOR_1.rawValue, //EPOS2_COLOR_1.rawValue,
            mode: EPOS2_MODE_MONO.rawValue,
            halftone:EPOS2_HALFTONE_DITHER.rawValue,
            brightness:Double(EPOS2_PARAM_DEFAULT),
            compress:EPOS2_COMPRESS_AUTO.rawValue)

        print("logo width", logo.size.width / 4)
        print("logo height", logo.size.height / 5)

        
        // Add cut to end the print
        result = printer!.addCut(EPOS2_CUT_FEED.rawValue)
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
            print("error")
            return false
        }
        
        
        if result != EPOS2_SUCCESS.rawValue {
            printer!.clearCommandBuffer()
            print("Error creating Receipt data")
            // --------1.----------
            print("Error adding data to printer")
            return false
        }

        return true
    }
    
  
    @discardableResult
    func initializePrinterObject() -> Bool
    {
        printer = Epos2Printer(printerSeries: valuePrinterSeries.rawValue, lang: valuePrinterModel.rawValue)
        
        if printer == nil {
            return false
        }
        printer!.setReceiveEventDelegate(self)
        
        return true
    }
    
    private func finalizePrinterObject() {
        if printer == nil {
            return
        }

        printer!.setReceiveEventDelegate(nil)
        printer = nil
    }
    
    
    //fix this
    private func connectPrinter() -> Bool {
        var result: Int32 = EPOS2_SUCCESS.rawValue
        
        if printer == nil {
            return false
        }
        
        let printerName = UserDefaults.standard.value(forKeyPath: "printer") as! String
        print("Printer is", printerName)
        result = printer!.connect(printerName, timeout:Int(EPOS2_PARAM_DEFAULT))
        if result != EPOS2_SUCCESS.rawValue {
            print("Error connecting to printer")
       //     MessageView.showErrorEpos(result, method:"connect")
            return false
        }
        
        return true
    }
    
    private func disconnectPrinter() {
        var result: Int32 = EPOS2_SUCCESS.rawValue
        
        if printer == nil {
            return
        }
        
        result = printer!.disconnect()
        if result != EPOS2_SUCCESS.rawValue {
            DispatchQueue.main.async(execute: {
           //     MessageView.showErrorEpos(result, method:"disconnect")
            })
        }

        printer!.clearCommandBuffer()
    }
    
     func onPtrReceive(_ printerObj: Epos2Printer!, code: Int32, status: Epos2PrinterStatusInfo!, printJobId: String!) {
     //   MessageView.showResult(code, errMessage: makeErrorMessage(status))
        
        
        print("Error code: ",code, status)
        
        dispPrinterWarnings(status)
        updateButtonState(true)
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(execute: {
            self.disconnectPrinter()
            })
    }
    
    private func dispPrinterWarnings(_ status: Epos2PrinterStatusInfo?) {
        if status == nil {
            return
        }
        
        
        if status!.paper == EPOS2_PAPER_NEAR_END.rawValue {
            print("Paper near end")
        }
        
        
        
        if status!.batteryLevel == EPOS2_BATTERY_LEVEL_1.rawValue {
            print("Battery level low")
        }
    }
    
    //4.
    func discoveryView(_ sendor: DiscoveryViewController, onSelectPrinterTarget target: String) {
        print("hello")
    }
    

}

extension TakePhotoViewController : ImagePickerDelegate
{
    func didSelect(image: UIImage?) {
        selectedPic = image
        
        
        if runPrinterReceiptSequence(pic: image!)
        {
            print("Completed")
            //show overlay or success, else show error
        }
//        createReceiptData(imageName: image!)
//        printData()
//        if !printData() {
//            return false
//        }
       }
  
    
    
}
