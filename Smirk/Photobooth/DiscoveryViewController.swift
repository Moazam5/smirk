//
//  DiscoveryViewController.swift
//  Smirk
//
//  Created by Moazam Mir on 3/25/21.
//

import UIKit

class DiscoveryViewController: UITableViewController , Epos2DiscoveryDelegate{

    fileprivate var printerList: [Epos2DeviceInfo] = []
    fileprivate var filterOption: Epos2FilterOption = Epos2FilterOption()
    let cellId = "cellId"

    let defaults  = UserDefaults.standard
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        filterOption.deviceType = EPOS2_TYPE_PRINTER.rawValue

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        let result = Epos2Discovery.start(filterOption, delegate: self)
        
        if result != EPOS2_SUCCESS.rawValue {
            
            //ShowMsg showErrorEpos(result, method: "start")
            print("Error in view did appear")
        }
        connectDevice()
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        while Epos2Discovery.stop() == EPOS2_ERR_PROCESSING.rawValue {
            // retry stop function
        }
        printerList.removeAll()
    }
    
   


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowNumber: Int = 0
        if section == 0 {
            rowNumber = printerList.count
        }
        else {
            rowNumber = 1
        }
        return rowNumber
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellId)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: cellId)
        }
        
        if indexPath.section == 0 {
            if indexPath.row >= 0 && indexPath.row < printerList.count {
                cell!.textLabel?.text = printerList[indexPath.row].deviceName
                print("Target is:",printerList[indexPath.row].target)
                cell!.detailTextLabel?.text = printerList[indexPath.row].target
            }
        }
        else {
            cell!.textLabel?.text = "other..."
            cell!.detailTextLabel?.text = ""
        }
        
        return cell!
    }
    
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            defaults.setValue(printerList[indexPath.row].target, forKey: "printer")

            dismiss(animated: true, completion: nil)
            
//            if delegate != nil {
//                defaults.setValue(printerList[indexPath.row].target, forKey: "printer")
//                delegate!.discoveryView(self, onSelectPrinterTarget: printerList[indexPath.row].target)
//                delegate = nil
//
//                navigationController?.popToRootViewController(animated: true)
//            }
//        }
//        else {
//            performSelector(onMainThread: #selector(DiscoveryViewController.connectDevice), with:self, waitUntilDone:false)
//        }
    }
   }

    @objc func connectDevice() {
        Epos2Discovery.stop()
        printerList.removeAll()
        
        let btConnection = Epos2BluetoothConnection()
        let BDAddress = NSMutableString()
        let result = btConnection?.connectDevice(BDAddress)
        print("BD Address is ", BDAddress)
        print("result is ", result)
        print("EPOS Success is", EPOS2_SUCCESS.rawValue)
        if result == EPOS2_SUCCESS.rawValue {
          //  defaults.setValue(BDAddress as String, forKey: "printer")

            print("Success")
//            delegate?.discoveryView(self, onSelectPrinterTarget: BDAddress as String)
//            delegate = nil
         //   self.navigationController?.popToRootViewController(animated: true)
        }
        else {
            Epos2Discovery.start(filterOption, delegate:self)
            tableView.reloadData()
        }
    }
    func onDiscovery(_ deviceInfo: Epos2DeviceInfo!) {
        printerList.append(deviceInfo)
        print("ondiscovery called")
        tableView.reloadData()
    }
}
