//
//  DiscoveryViewDelegate.swift
//  Smirk
//
//  Created by Moazam Mir on 3/25/21.
//

import Foundation

protocol DiscoveryViewDelegate {
    func discoveryView(_ sendor: DiscoveryViewController, onSelectPrinterTarget target:String)
}
