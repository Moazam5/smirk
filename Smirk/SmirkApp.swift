//
//  SmirkApp.swift
//  Smirk
//
//  Created by Moazam Mir on 1/29/21.
//

import SwiftUI

@main
struct SmirkApp: App {
    let defaults = UserDefaults.standard
    var body: some Scene {
        WindowGroup {
            if defaults.bool(forKey:Constants.Keys.isInitialSetup)
            {
                
                PhotoOnlyView()
              ///r  ContentView()

                
            }
            else
            {
                 StartView()
            }
        }
    }
}
