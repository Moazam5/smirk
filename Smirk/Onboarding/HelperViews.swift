//
//  HelperViews.swift
//  Smirk
//
//  Created by Moazam Mir on 5/22/21.
//

import SwiftUI

struct HeadingText: View {
    let text: String

    var body: some View {
        Text(text)
            .font(.system(size: 72))
            .fontWeight(.bold)
            .foregroundColor(Color.white)
            .frame(maxWidth : .infinity)
    }
}

struct ProgressHeader: View {

    let firstLabel: String
    let isFirstSelected: Bool
    let secondLabel: String
    let isSecondSelected: Bool

    var body: some View {
        HStack(spacing: 0) {
            ProgressBarHead(label: firstLabel, isSelected: isFirstSelected)
            RoundedRectangle(cornerRadius: 0)
                .frame(maxWidth: .infinity, maxHeight:  8)
                .foregroundColor(Color.white)
            ProgressBarHead(label: secondLabel, isSelected: isSecondSelected)
        }
    }
}
struct ProgressBarHead: View {
    let label: String
    let isSelected: Bool

    var body: some View {

            Text(label)
                .font(.system(size : 25))
                .frame(width: 150, height: 60, alignment: .center)
                .background(isSelected ? Color.white : Color.clear)
                .foregroundColor(isSelected ? .black : .white)
                .cornerRadius(60)
                .overlay(
                       RoundedRectangle(cornerRadius: 50)
                        .stroke(isSelected ? Color.clear : Color.white, lineWidth: 6) )

    }
}

struct SubHeadingText: View {
    let text: String

    var body: some View {
        Text(text)
            .font(.system(size : 40))
            .fontWeight(.medium)
            .foregroundColor(Color.white)
    }
}

struct HelperViews_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            HeadingText(text: "Hello")
                .background(Color.blue)
            Spacer()
                .frame(height: 50)
            ProgressBarHead(label: "Button", isSelected: false) 
                .background(Color.black)
            Spacer()
                .frame(height: 50)

            ProgressHeader(firstLabel: "Setup Photo", isFirstSelected: true, secondLabel: "Setup Printer", isSecondSelected: false)
        }.preferredColorScheme(.light)

    }
}
