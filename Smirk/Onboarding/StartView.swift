//
//  ContentView.swift
//  Smirk
//
//  Created by Moazam Mir on 1/29/21.
//

import SwiftUI

struct StartView: View {
    @State var cameraClicked: Bool = false
    @State private var showCaptureImageView: Bool = false
    @State private var image: Image? = nil
    private let cornerRadius: CGFloat = 14.0

    var body: some View {
        VStack(alignment : .center) {
            Spacer()

            HeadingText(text: "Welcome to")
            HeadingText(text: "Smirk")
            Spacer()
            SubHeadingText(text: "Press the Start Button to Get Started")
            Spacer()

            HStack {
                Button(action: {
                    NSLog("Camera Button clicked")
                    cameraClicked.toggle()}) {
                        Text("Start Photobooth")
                }
                .frame(width: 600, height: 80, alignment: .center)
                .background(Color.white)
                .cornerRadius(cornerRadius)
                .shadow(color: Color.white.opacity(0.20), radius: 4, x: 0, y: 4)
                .sheet(isPresented: self.$cameraClicked){
                    FirstSetupView()
                }
            }
            .foregroundColor(.gray)
            .font(.system(size: 24))
            Spacer()

        }
        .background(LinearGradient(gradient: Gradient(colors: [.white, .blue]), startPoint: .top, endPoint: .bottom))
        .ignoresSafeArea()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        StartView()
    }
}

